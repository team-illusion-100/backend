package be.kdg.machiavelli.services;

import be.kdg.machiavelli.exceptions.MyException;
import be.kdg.machiavelli.model.dto.ChangeCaracterDto;
import be.kdg.machiavelli.model.dto.DtoMapper;
import be.kdg.machiavelli.model.dto.GameDto;
import be.kdg.machiavelli.model.dto.PlayerDto;
import be.kdg.machiavelli.model.game.artifacts.CharacterCard;
import be.kdg.machiavelli.model.game.logic.GameStatus;
import be.kdg.machiavelli.model.game.logic.JoinType;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.game.logic.MVPlayer;
import be.kdg.machiavelli.model.security.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GameServiceTest {
    @Autowired
    PlayerService playerService;

    @Mock
    DtoMapper dtoMapper;

    @Mock
    CharacterCard characterCard;

    @Autowired
    private CharacterCardService characterCardService;

    @Autowired
    UserService userDetailsService;

    @Autowired
    GameService gameService;

    @Mock
    CharacterCardService characterCardServiceMock;

    @Mock
    MVGame gameMock;

    @InjectMocks
    GameDto gameDtoMock;

    @Mock
    GameService gameServiceMock;

    @InjectMocks
    PlayerDto playerDtoMock;

    @Mock
    ChangeCaracterDto changeCaracterDto;



    @Before
    public void setup() throws MyException {
        MockitoAnnotations.initMocks(this);
        PlayerDto[] pl ={playerDtoMock};
        gameDtoMock.setPlayers(pl);
        gameDtoMock = dtoMapper.toDto(gameMock);
        when(gameServiceMock.determineKing(gameMock.getId())).thenReturn(1);
        when(gameServiceMock.nextPlayer(gameDtoMock, 1)).thenReturn(2);
//        when(gameDtoMock.getPlayers()[0].getEndGamePoints()).thenReturn(2);
//        when(gameServiceMock.nextCharacter(gameDtoMock).getCharacterTurn()).thenReturn(1);


    }

    @Test
    @Transactional
    public void getGameOverviewWithWrongName() {
        MVGame game = new MVGame("testCase", GameStatus.ONGOING, 60, 8, 60, JoinType.OPEN);
        gameService.saveGame(game);
        User u = new User();
        u.setUsername("testCase");
        u.setAvatar("Avatar1");
        userDetailsService.saveUser(u);
        MVPlayer player = new MVPlayer(u, 1, true);
        player = playerService.save(player);
        List<MVPlayer> playerList = new ArrayList<>();
        playerList.add(player);
        u.setPlayers(playerList);
        userDetailsService.saveUser(u);
        game.getPlayers().add(player);
        game.setName("testCaseGameName");
        gameService.saveGame(game);
        List<GameDto> gameDtoList = gameService.getGameOverview("testCase");
        assertNotEquals("testCaseGameNameFalse", gameService.getGameOverview("testCase").get(0).getGameName());
    }

    @Test
    @Transactional
    public void getGameOverviewWithRightNameAndAmount() {
        MVGame game = new MVGame("testCase", GameStatus.ONGOING, 60, 8, 60, JoinType.OPEN);
        gameService.saveGame(game);
        User u = new User();
        u.setUsername("testCase");
        u.setAvatar("Avatar1");
        userDetailsService.saveUser(u);

        MVPlayer player = new MVPlayer(u, 1, true);
        player = playerService.save(player);

        List<MVPlayer> playerList = new ArrayList<>();
        playerList.add(player);
        u.setPlayers(playerList);
        userDetailsService.saveUser(u);
        game.getPlayers().add(player);
        game.setName("testCaseGameName");
        gameService.saveGame(game);

        List<GameDto> gameDtoList = gameService.getGameOverview("testCase");
        assertEquals(1, gameService.getGameOverview("testCase").size());
        assertEquals("testCaseGameName", gameService.getGameOverview("testCase").get(0).getGameName());
    }

    @Test
    @Transactional
    public void getGameOverviewRightAmount() {
        MVGame game = new MVGame("yassinegg7", GameStatus.ONGOING, 60, 8, 60, JoinType.OPEN);
        gameService.saveGame(game);

        User u = new User();
        u.setUsername("yassinegg7");
        //u.setId(15111L);
        u.setAvatar("Avatar1");
        userDetailsService.saveUser(u);

        List<User> userList = new ArrayList<>();
        userList.add(userDetailsService.findByUsername("yassinegg7"));

        MVPlayer player = new MVPlayer(u, 1, true);
        MVPlayer player2 = new MVPlayer(u, 1, true);
        MVPlayer player3 = new MVPlayer(u, 1, true);
        //player.setId(19999);
        player.setActiveUserName("yassinegg7");
        player = playerService.save(player);
        player2 = playerService.save(player2);
        player3 = playerService.save(player3);

        List<MVPlayer> playerList = new ArrayList<>();
        playerList.add(player);
        playerList.add(player2);
        playerList.add(player3);

        u.setPlayers(playerList);
        userDetailsService.saveUser(u);

        game.getPlayers().add(player);
        game.getPlayers().add(player2);
        game.getPlayers().add(player3);
        gameService.saveGame(game);

        //gameService.getGameOverview("yassineggALALALALALA");
        assertEquals(3, gameService.getGameOverview("yassinegg7").size());

    }

    @Test
    @Transactional
    public void getGameOverviewWrongAmount() {
        MVGame game = new MVGame("test", GameStatus.ONGOING, 60, 8, 60, JoinType.OPEN);
        gameService.saveGame(game);
        User u = new User();
        u.setUsername("test");
        u.setAvatar("Avatar1");
        userDetailsService.saveUser(u);
        List<User> userList = new ArrayList<>();
        userList.add(userDetailsService.findByUsername("test"));
        MVPlayer player = new MVPlayer(u, 1, true);
        player.setActiveUserName("test");
        player = playerService.save(player);
        List<MVPlayer> playerList = new ArrayList<>();
        playerList.add(player);
        u.setPlayers(playerList);
        userDetailsService.saveUser(u);
        game.getPlayers().add(player);
        gameService.saveGame(game);
        assertNotEquals(3, gameService.getGameOverview("test").size());
    }

    @Test
    @Transactional
    public void searchGamesPRIVATE() {
        int amount = gameService.searchGames().size();
        MVGame game = new MVGame("test", GameStatus.LOBBY, 60, 8, 60, JoinType.PRIVATE);
        gameService.saveGame(game);
        assertNotEquals(amount + 1, gameService.searchGames().size());
    }

    @Test
    @Transactional
    public void searchGamesLOBBY() {
        int amount = gameService.searchGames().size();
        MVGame game = new MVGame("test", GameStatus.LOBBY, 60, 8, 60, JoinType.OPEN);
        gameService.saveGame(game);
        assertEquals(amount + 1, gameService.searchGames().size());
    }

    @Test
    @Transactional
    public void searchGamesONGOING() {
        int amount = gameService.searchGames().size();
        MVGame game = new MVGame("test", GameStatus.ONGOING, 60, 8, 60, JoinType.PRIVATE);
        gameService.saveGame(game);
        assertNotEquals(amount + 1, gameService.searchGames().size());
    }

    @Test
    @Transactional
    public void searchGamesOPEN() {
        int amount = gameService.searchGames().size();
        MVGame game = new MVGame("test", GameStatus.LOBBY, 60, 8, 60, JoinType.OPEN);
        gameService.saveGame(game);
        assertEquals(amount + 1, gameService.searchGames().size());
    }

    @Test
    @Transactional
    public void getCharactersOfGameWhithRightAmountAndId() throws MyException {
        MVGame game = new MVGame("test", GameStatus.LOBBY, 60, 8, 60, JoinType.OPEN);
        List<CharacterCard> characterCardList = new ArrayList<>();
        characterCardList.add(characterCard);
        characterCardList.add(characterCard);
        characterCardList.add(characterCard);
        characterCardList.add(characterCard);
        game.setCharacters(characterCardList);
        gameService.saveGame(game);
        List<CharacterCard> characterCardListReturned = gameService.getCharactersOfGame(game.getId());
        assertEquals(4, characterCardListReturned.size());
    }
//

    @Test(expected = Exception.class)
    @Transactional
    public void getCharactersOfGameWrongId() throws MyException {
        MVGame game = new MVGame("test", GameStatus.LOBBY, 60, 8, 60, JoinType.OPEN);
        List<CharacterCard> characterCardList = new ArrayList<>();
        characterCardList.add(characterCard);
        game.setCharacters(characterCardList);
        gameService.saveGame(game);
        List<CharacterCard> characterCardListReturned = gameService.getCharactersOfGame(-1);
        assertNotEquals(-1, characterCardListReturned.size());
    }



    @Test
    @Transactional
    public void determineKing() throws MyException {
        gameServiceMock.saveGame(gameMock);
        assertEquals(gameServiceMock.determineKing(gameMock.getId()), 1);
    }

    @Test
    @Transactional
    public void determineKingWrongNumberReturns() throws MyException {
        gameServiceMock.saveGame(gameMock);
        assertNotEquals(gameServiceMock.determineKing(gameMock.getId()), 2);
    }

    @Test
    @Transactional
    public void nextPlayer() {
        gameServiceMock.saveGame(gameMock);
        assertEquals(gameServiceMock.nextPlayer(gameDtoMock, 1), 2);
    }
    @Test
    @Transactional
    public void nextPlayerShouldBeFirst() {
        gameServiceMock.saveGame(gameMock);
        assertEquals(gameServiceMock.nextPlayer(gameDtoMock, -1), 0);
    }

    @Test
    @Transactional
    public void nextPlayerWrong() {
        gameServiceMock.saveGame(gameMock);
        assertNotEquals(gameServiceMock.nextPlayer(gameDtoMock, 1), 3);
    }

    @Test
    @Transactional
    public void nextCharacter() {
        gameServiceMock.saveGame(gameMock);
        GameDto gameDto = dtoMapper.toDto(gameMock);
        GameDto g;
//        int playturn = gameDto.getPlayerTurn();
        int charturn = gameDto.getCharacterTurn();
        g = gameServiceMock.nextCharacter(gameDto);
        GameDto g2 = gameServiceMock.nextCharacter(gameDto);
        assertEquals(g.getPlayerTurn(), 0);
        assertEquals(gameServiceMock.nextCharacter(gameDto).getCharacterTurn(), charturn);
    }

    @Test
    public void assignCharacterToPlayer(){
        List cl = new ArrayList();
        cl.add(changeCaracterDto);
        characterCardServiceMock.saveAll(cl);
        GameDto game = changeCaracterDto.getGameDto();
        GameDto game2 = gameServiceMock.assignCharacterToPlayer(changeCaracterDto);
        assertNotEquals(game, game2);
    }

    @Test
    public void assignCharacterToPlayerCheckIfPlayerHasBeenChosen(){
        List cl = new ArrayList();
        cl.add(changeCaracterDto);
        characterCardServiceMock.saveAll(cl);
        GameDto game = changeCaracterDto.getGameDto();
        assertTrue(game.isPlayersChosen());
    }

    @Test
    public void resetRound(){
        assertEquals(0, gameServiceMock.resetRound(gameDtoMock).getCharacterTurn());
    }

    @Test
    public void resetRoundResetFail(){
        assertNotEquals(1, gameServiceMock.resetRound(gameDtoMock).getCharacterTurn());
    }

    @Test
    @Transactional
    public void endgame(){
      //GameDto gameDto = dtoMapper.toDto(gameService.searchGames().get(1)) ;
      //int before =  gameDto.getPlayers()[0].getEndGamePoints();
      // gameService.endgame(gameDto);
      //int after = gameDto.getPlayers()[0].getEndGamePoints();
      //assertNotEquals(before, after);
       //GameDto gameDto = dtoMapper.toDto(gameService.searchGames().get(1)) ;
        gameServiceMock.saveGame(gameMock);
       int before =  gameDtoMock.getPlayers()[0].getEndGamePoints();
        gameServiceMock.endgame(gameDtoMock);
       int after = gameDtoMock.getPlayers()[0].getEndGamePoints();
       assertNotEquals(before, after);
    }

}