package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.MachiavelliApplication;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.services.GameService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = MachiavelliApplication.class)
public class GameControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private GameService gameService;

    @Mock
    private MVGame game;

    private MockMvc mockMvc;

    private static final String CLIENT_ID = "jwtclientid";
    private static final String CLIENT_SECRET = "XY7kmzoNzl100";

    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    private static final String LOGIN = "admin.admin";

    @Before
    @Transactional
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
    }

    private String obtainAccessToken(String username, String password) throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", username);
        params.add("password", password);
        ResultActions result = mockMvc.perform(post("/oauth/token")
                .params(params)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE));
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(result.andReturn().getResponse().getContentAsString()).get("access_token").toString();
    }


    @Test
    public void getGameOverview() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
       // Principal p = new Principal() {
       //     @Override
       //     public String getName() {
       //         return "yassine.boffuzeya";
       //     }
       // };

        mockMvc.perform(get("/gameOverview")
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    public void getGameOverviewFail() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
        mockMvc.perform(get("/gameOverview")
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print());

    }
    @Test
    public void searchGame() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
        mockMvc.perform(get("/searchGame")
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    public void getGameFail() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");

        mockMvc.perform(get("/getGame/1")
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print())
                .andExpect(status().isIAmATeapot());
    }

    @Test
    @Transactional
    public void getGame() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
       int gameId = gameService.searchGames().get(0).getId();

        mockMvc.perform(get("/getGame/" + gameId)
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void getLiteGame() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
       int gameId = gameService.searchGames().get(0).getId();

        mockMvc.perform(get("/getLiteGame/" + gameId)
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void addCoinsFail() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
       //int gameId = gameService.searchGames().get(0).getId();

        mockMvc.perform(get("/app/addCoins/")
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void addCoins() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
       //int gameId = gameService.searchGames().get(0).getId();

        mockMvc.perform(get("/app/addCoins/")
                .header("Authorization", "Bearer " + accessToken))
                .andDo(print());
    }





}