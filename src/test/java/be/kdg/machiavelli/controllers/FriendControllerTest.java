package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.MachiavelliApplication;
import be.kdg.machiavelli.services.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = MachiavelliApplication.class)
public class FriendControllerTest {
    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Autowired
    private UserService userService;

    private MockMvc mockMvc;

    private static final String CLIENT_ID = "jwtclientid";
    private static final String CLIENT_SECRET = "XY7kmzoNzl100";

    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    private static final String LOGIN = "admin.admin";
    //private static final String NAME = "Yassine";

    @Before
    @Transactional
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();


    }


    private String obtainAccessToken(String username, String password) throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).addFilter(springSecurityFilterChain).build();
        final MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("grant_type", "password");
        params.add("username", username);
        params.add("password", password);
        ResultActions result = mockMvc.perform(post("/oauth/token")
                .params(params)
                .with(httpBasic(CLIENT_ID, CLIENT_SECRET))
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(CONTENT_TYPE));
        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(result.andReturn().getResponse().getContentAsString()).get("access_token").toString();
    }


    @Test
    @Transactional
    public void sendFriendsSuccessfully() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
        mockMvc.perform(post("/sendFriendRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test(expected = Exception.class)
    @Transactional
    public void addFriendsUnSuccessfully() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
        mockMvc.perform(post("/acceptRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("fdsqdsffsd")
                .accept(CONTENT_TYPE))
                .andDo(print());
    }


    @Test(expected = AssertionError.class)
    @Transactional
    public void getFriendsUnsuccessfully() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");

        mockMvc.perform(get("/getFriends")
                .header("Authorization", "Bearer " + accessToken)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andDo(print())
                .andExpect(jsonPath("$[0].userName", is("yassine.bouzeya")));
    }

    @Test
    @Transactional
    public void getFriendsSuccessfully() throws Exception {
       final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
        mockMvc.perform(post("/sendFriendRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());

        final String accessToken2 = obtainAccessToken("yassine.bouzeya", "jwtpass");

        mockMvc.perform(post("/acceptRequest")
               .header("Authorization", "Bearer " + accessToken2)
               .contentType(CONTENT_TYPE)
               .content(LOGIN)
               .accept(CONTENT_TYPE))
               .andDo(print())
               .andExpect(status().isOk());
//
        mockMvc.perform(get("/getFriends")
                .header("Authorization", "Bearer " + accessToken)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andDo(print())
                .andExpect(jsonPath("$[0].userName", is("yassine.bouzeya")));
    }

    @Test
    @Transactional
    public void getReceivedFriendRequestsEmpty() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
        mockMvc.perform(get("/getReceivedFriendRequests")
                .header("Authorization", "Bearer " + accessToken)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void getReceivedFriendRequestsAndSendFriendRequestSuccessfull() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");
        mockMvc.perform(post("/sendFriendRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/getSentFriendRequests")
                .header("Authorization", "Bearer " + accessToken)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andDo(print())
                .andExpect(jsonPath("$.[0].receiver", is("yassine.bouzeya")));
    }


    @Test
    @Transactional
    public void acceptRequest() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");

        mockMvc.perform(post("/sendFriendRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());


        final String accessToken2 = obtainAccessToken("yassine.bouzeya", "jwtpass");

        mockMvc.perform(post("/acceptRequest")
                .header("Authorization", "Bearer " + accessToken2)
                .content("admin.admin")
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(get("/getFriends")
                .header("Authorization", "Bearer " + accessToken)
                .content("admin.admin")
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(content().contentType(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].userName", is("yassine.bouzeya")));

    }

    @Test
    @Transactional
    public void denyRequest() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");

        mockMvc.perform(post("/sendFriendRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());


        final String accessToken2 = obtainAccessToken("yassine.bouzeya", "jwtpass");

        mockMvc.perform(post("/denyRequest")
                .header("Authorization", "Bearer " + accessToken2)
                .accept(CONTENT_TYPE)
                .content("admin.admin"))
                .andExpect(status().isOk());
    }


    @Test
    @Transactional
    public void removeRequest() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");

        mockMvc.perform(post("/sendFriendRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(post("/removeRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Transactional
    public void removeFriendfail() throws Exception {
        final String accessToken = obtainAccessToken(LOGIN, "jwtpass");

        mockMvc.perform(post("/sendFriendRequest")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(CONTENT_TYPE)
                .content("yassine.bouzeya")
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());


        final String accessToken2 = obtainAccessToken("yassine.bouzeya", "jwtpass");

        mockMvc.perform(post("/acceptRequest")
                .header("Authorization", "Bearer " + accessToken2)
                .content(LOGIN)
                .accept(CONTENT_TYPE))
                .andExpect(content().contentType(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isOk());

        mockMvc.perform(post("/removeFriend")
                .header("Authorization", "Bearer " + accessToken2)
                .content(LOGIN)
                .accept(CONTENT_TYPE))
                .andDo(print())
                .andExpect(status().isUnsupportedMediaType());
    }
}