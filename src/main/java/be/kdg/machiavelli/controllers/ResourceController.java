package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.services.securityImpl.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * HttpStatus.I_AM_A_TEAPOT indicates an Exception happened in the chain of controller - service - repo
 * The Exception gets logged in this class (the Controller)
 */

@RestController
@RequestMapping("/springjwt")
public class ResourceController {
    @Autowired
    private GenericService userService;


    @RequestMapping(value ="/users", method = RequestMethod.GET)
    //@PreAuthorize("hasAuthority('ADMIN_USER')")
    public ResponseEntity getUsers(){
        try {
            return new ResponseEntity(this.userService.findAllUsers(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }
}