package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.ChatMessageDto;
import be.kdg.machiavelli.model.dto.PlayerDto;
import be.kdg.machiavelli.services.ChatMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class ChatController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);
    private final SimpMessagingTemplate template;
    private ChatMessageService chatMessageService;

    public ChatController(ChatMessageService chatMessageService, SimpMessagingTemplate template) {
        this.chatMessageService = chatMessageService;
        this.template = template;
    }

    @PostMapping("sendMessage")
    public ResponseEntity sendMessage(Principal principal, @RequestBody ChatMessageDto message) {
        try {
            ChatMessageDto messageDto = chatMessageService.sendMessage(principal.getName(), message);
            template.convertAndSend("/r/chat/" + messageDto.getGame().getId(), messageDto);
            for (PlayerDto p : messageDto.getGame().getPlayers()) {
                if (!(p.getUserName().equals(principal.getName())))
                    template.convertAndSendToUser(p.getUserName(), "/r/chatNotif", messageDto);
            }

            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in sendMessage: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }
}
