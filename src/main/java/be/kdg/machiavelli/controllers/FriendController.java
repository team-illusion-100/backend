package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.model.dto.FriendDto;
import be.kdg.machiavelli.model.dto.FriendRequestDto;
import be.kdg.machiavelli.services.FriendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import be.kdg.machiavelli.services.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * HttpStatus.I_AM_A_TEAPOT indicates an Exception happened in the chain of controller - service - repo
 * The Exception gets logged in this class (the Controller)
 */

@RestController
public class FriendController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FriendController.class);

    private final SimpMessagingTemplate template;
    private FriendService friendService;
    private UserService userService;

    public FriendController(FriendService friendService, UserService userService, SimpMessagingTemplate template) {
        this.friendService = friendService;
        this.userService = userService;
        this.template = template;
    }

    @GetMapping("getFriends")
    public ResponseEntity getFriends(Principal principal) {
        try {
            return new ResponseEntity(this.userService.getFriendDtos(principal.getName()), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getFriends: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("getReceivedFriendRequests")
    public ResponseEntity getReceivedFriendRequests(Principal principal) {
        try {
            return new ResponseEntity(this.friendService.getReceivedFriendRequests(principal.getName()), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getReceivedFriendRequests: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @GetMapping("getSentFriendRequests")
    public ResponseEntity getSentFriendRequests(Principal principal) {
        try {
            return new ResponseEntity(this.friendService.getSentFriendRequests(principal.getName()), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in getSentFriendRequests: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @PostMapping("sendFriendRequest")
    public ResponseEntity sendFriendRequest(Principal principal, @RequestBody String friendName) {
        try {
            FriendRequestDto friendRequestDto = this.friendService.sendFriendRequest(principal.getName(), friendName);
            if (friendRequestDto != null) {
                this.template.convertAndSendToUser(friendName, "/r/friendRequest", friendRequestDto);
                return new ResponseEntity(friendRequestDto, HttpStatus.OK);
            } else return new ResponseEntity(null, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in sendFriendRequest: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }

    }

    @PostMapping("acceptRequest")
    public ResponseEntity acceptRequest(Principal principal, @RequestBody String acceptedFriendName) {
        try {
            FriendDto acceptedFriendDto = this.friendService.acceptRequest(principal.getName(), acceptedFriendName);
            FriendDto accptingFriendDto = this.friendService.getFriendByUsername(principal.getName());
            this.template.convertAndSendToUser(acceptedFriendName, "/r/acceptRequest", accptingFriendDto);
            return new ResponseEntity(acceptedFriendDto, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in acceptRequest: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }
    @PostMapping("denyRequest")
    public ResponseEntity denyRequest(Principal principal, @RequestBody String deniedFriendName) {
        try {
            FriendDto denyingFriendDto = this.friendService.denyRequest(principal.getName(), deniedFriendName);
            this.template.convertAndSendToUser(deniedFriendName, "/r/denyRequest", denyingFriendDto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in denyRequest: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }
    @PostMapping("removeRequest")
    public ResponseEntity removeRequest(Principal principal, @RequestBody String removedFriendName) {
        try {
            FriendDto removingFriendDto = this.friendService.removeRequest(principal.getName(), removedFriendName);
            this.template.convertAndSendToUser(removedFriendName, "/r/removeRequest", removingFriendDto);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in removeRequest: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }

    @PostMapping("removeFriend")
    public ResponseEntity removeFriend(Principal principal, @RequestBody FriendDto friendToRemove) {
        try {
            FriendDto deletingFriend = this.friendService.removeFriend(principal.getName(), friendToRemove.getUserName());
            this.template.convertAndSendToUser(friendToRemove.getUserName(), "/r/deleteFriend", deletingFriend);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Exception in removeFriend: " + e.getMessage());
            return new ResponseEntity(HttpStatus.I_AM_A_TEAPOT);
        }
    }
//    @PostMapping("createFriends")
//    public void createFriends(){
//        this.userService.createFriends();
//    }
}
