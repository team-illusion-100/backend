package be.kdg.machiavelli.controllers;

import be.kdg.machiavelli.services.BuildingService;
import be.kdg.machiavelli.services.CharacterService;
import be.kdg.machiavelli.services.InitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {
    @Autowired
    private InitService initService;
    @Autowired
    private CharacterService characterService;
    @Autowired
    private BuildingService buildingService;

    @GetMapping("/")
    public String goToIndex(Model model){
        model.addAttribute("moordenaar",characterService.getCharacter(6));
        return "index";
    }

    @GetMapping("/initCharacters")
    public String initCharacters(){
        this.initService.persistCharacters();
        return "index";
    }


    @GetMapping("/initBuildings")
    public String initBuildings(){
        this.initService.persistBuildings();
        return "redirect:/";
    }

}
