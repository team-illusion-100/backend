package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.security.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface UserRepo extends CrudRepository<User, Long> {
    User findByUsername(String username);
    User findById(long id);
    User findByEmail(String email);
    List<User> findAllByUsernameContains(String username);

    //List<User> findUsersByPlayer(int id);
}
