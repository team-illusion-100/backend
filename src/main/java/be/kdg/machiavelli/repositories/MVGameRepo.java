package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.game.logic.GameStatus;
import be.kdg.machiavelli.model.game.logic.JoinType;
import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.game.logic.MVPlayer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MVGameRepo extends JpaRepository<MVGame, Integer> {
    MVGame findByPlayersContains(MVPlayer player);
    MVGame findByPlayersId(int id);

    List<MVGame> findAllByJoinTypeAndStatus(JoinType joinType, GameStatus gameStatus);
    MVGame findById(int id);
}
