package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.game.logic.MVPlayer;
import be.kdg.machiavelli.model.security.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MVPlayerRepo extends JpaRepository<MVPlayer, Integer> {
    List<MVPlayer> findAllByUsersContains(User user);

}
