package be.kdg.machiavelli.repositories;

import be.kdg.machiavelli.model.user.ChatMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatMessageRepo extends JpaRepository<ChatMessage, Integer>
{

}
