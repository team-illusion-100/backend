package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;
import be.kdg.machiavelli.model.game.artifacts.MVBuilding;
import be.kdg.machiavelli.repositories.BuildingCardRepo;
import be.kdg.machiavelli.repositories.MVBuildingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BuildingCardService {
    @Autowired
    private BuildingCardRepo buildingCardRepo;

    public BuildingCard saveBuilding(BuildingCard buildingCard){
        return this.buildingCardRepo.save(buildingCard);
    }

    public BuildingCard getOneBuildingCard(int id){
        return this.buildingCardRepo.getOne(id);
    }

    public List<BuildingCard> findAll(){
        return buildingCardRepo.findAll();
    }

    public List<BuildingCard> saveAll(List<BuildingCard> buildingCards){
        return  buildingCardRepo.saveAll(buildingCards);
    }

    public BuildingCard discardBuilding(int id){
        BuildingCard buildingCard = this.buildingCardRepo.getOne(id);
        buildingCard.setDiscarded(true);
       return buildingCardRepo.save(buildingCard);
    }


    public void removeBuildingById(int id){
        buildingCardRepo.deleteById(id);
    }
    public void removeBuildingAll(){
        buildingCardRepo.deleteAll();
    }


}
