package be.kdg.machiavelli.services.securityImpl;

import be.kdg.machiavelli.model.security.User;


import java.util.List;

public interface GenericService {
    User findByUsername(String username);

    List<User> findAllUsers();

}
