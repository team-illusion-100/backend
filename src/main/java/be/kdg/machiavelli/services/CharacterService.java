package be.kdg.machiavelli.services;

import be.kdg.machiavelli.model.game.artifacts.MVCharacter;
import be.kdg.machiavelli.repositories.MVCharacterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CharacterService {
    @Autowired
    private MVCharacterRepo characterRepo;

    //this is only used for filling up the database
    public MVCharacter saveCharacter(MVCharacter character){
        return this.characterRepo.save(character);
    }

    public MVCharacter getCharacter(int id){
        return this.characterRepo.getOne(id);
    }

    public List<MVCharacter> findAll(){
        return this.characterRepo.findAll();
    };


}
