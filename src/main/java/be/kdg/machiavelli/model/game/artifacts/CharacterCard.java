package be.kdg.machiavelli.model.game.artifacts;

import javax.persistence.*;

@Entity
public class CharacterCard {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    private boolean isFaceUp;
    private boolean isKilled;
    private boolean isRobbed;
    private boolean isPlacedMiddle;
    @OneToOne
    private MVCharacter character;

    public CharacterCard(){}

    public CharacterCard(MVCharacter character) {
        this.isFaceUp = false;
        this.isKilled = false;
        this.isRobbed = false;
        this.isPlacedMiddle=false;
        this.character = character;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isFaceUp() {
        return isFaceUp;
    }

    public void setFaceUp(boolean faceUp) {
        isFaceUp = faceUp;
    }

    public boolean isKilled() {
        return isKilled;
    }

    public void setKilled(boolean killed) {
        isKilled = killed;
    }

    public boolean isRobbed() {
        return isRobbed;
    }

    public void setRobbed(boolean robbed) {
        isRobbed = robbed;
    }

    public MVCharacter getCharacter() {
        return character;
    }

    public void setCharacter(MVCharacter character) {
        this.character = character;
    }

    public boolean isPlacedMiddle() {
        return isPlacedMiddle;
    }

    public void setPlacedMiddle(boolean placedMiddle) {
        isPlacedMiddle = placedMiddle;
    }
}
