package be.kdg.machiavelli.model.game.logic;

import be.kdg.machiavelli.model.game.artifacts.CharacterCard;

import javax.persistence.*;
import java.util.List;

@Entity
public class Round {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    private int roundNR;
    @OneToOne
    private MVGame game;    //Can someone please check this one if it is needed I am not sure about this


    @OneToMany(targetEntity = Move.class,
            cascade = { CascadeType.REMOVE })
    List<Move> moves;

    public Round(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoundNR() {
        return roundNR;
    }

    public void setRoundNR(int roundNR) {
        this.roundNR = roundNR;
    }

    public MVGame getGame() {
        return game;
    }

    public void setGame(MVGame game) {
        this.game = game;
    }

    public List<Move> getMoves() {
        return moves;
    }

    public void setMoves(List<Move> moves) {
        this.moves = moves;
    }
}
