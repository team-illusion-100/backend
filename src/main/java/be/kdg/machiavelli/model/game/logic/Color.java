package be.kdg.machiavelli.model.game.logic;

public enum Color {
    YELLOW(0), BLUE(1), GREEN(2), RED(3), PURPLE(4), GREY(5);
    private int i;

    Color(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }
}
