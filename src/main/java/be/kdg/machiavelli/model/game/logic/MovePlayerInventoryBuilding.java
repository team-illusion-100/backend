package be.kdg.machiavelli.model.game.logic;

import be.kdg.machiavelli.model.game.artifacts.MVBuilding;

import javax.persistence.*;

@Entity
public class MovePlayerInventoryBuilding {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne
    private MovePlayerInventory playerInventory;
    @OneToOne
    private MVBuilding building;
    private boolean isBought;
    private boolean isDiscarded;
    public MovePlayerInventoryBuilding(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MovePlayerInventory getPlayerInventory() {
        return playerInventory;
    }

    public void setPlayerInventory(MovePlayerInventory playerInventory) {
        this.playerInventory = playerInventory;
    }

    public MVBuilding getBuilding() {
        return building;
    }

    public void setBuilding(MVBuilding building) {
        this.building = building;
    }

    public boolean isBought() {
        return isBought;
    }

    public void setBought(boolean bought) {
        isBought = bought;
    }

    public boolean isDiscarded() {
        return isDiscarded;
    }

    public void setDiscarded(boolean discarded) {
        isDiscarded = discarded;
    }
}
