package be.kdg.machiavelli.model.security;

import be.kdg.machiavelli.model.game.logic.MVPlayer;
import be.kdg.machiavelli.model.user.ChatMessage;
import be.kdg.machiavelli.model.user.Friend;
import be.kdg.machiavelli.model.user.Notification;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    @JsonIgnore
    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "friend_notif")
    private boolean friendRequestNotifications;

    @Column(name = "new_game_notif")
    private boolean inviteNotifications;

    @Column(name = "other_turn_notif")
    private boolean otherTurnNotifications;

    @Column(name = "your_turn_notif")
    private boolean myTurnNotifications;

    @Column(name = "turn_expiring_notif")
    private boolean turnIsExpiringNotifications;

    @Column(name = "chat_notif")
    private boolean chatNotifications;

    @Column(name = "is_public")
    private boolean isPublic;

    /**
     * Roles are being eagerly loaded here because
     * they are a fairly small collection of items.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", joinColumns
            = @JoinColumn(name = "user_id",
            referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",
                    referencedColumnName = "id"))
    private List<Role> roles;
    private String email;
    private LocalDateTime dateOfBirth;
    private int numberOfGames;
    private int numberOfWins;
    private double averageScore;
    private int topScore;
    private int replayLiveDays;
    private String avatar;
    @ManyToMany(targetEntity = MVPlayer.class,
            cascade = {CascadeType.REMOVE})
    private List<MVPlayer> players;

    @OneToMany(targetEntity = Notification.class,
            cascade = {CascadeType.REMOVE})
    private List<Notification> notifications;

    @OneToMany(targetEntity = ChatMessage.class,
            cascade = {CascadeType.REMOVE})
    private List<ChatMessage> chatMessages;

    @OneToMany(targetEntity = Friend.class,
            cascade = {CascadeType.REMOVE})
    private List<Friend> friends;

    public User() {
        this.notifications = new ArrayList<>();
        this.chatMessages = new ArrayList<>();
        this.friends = new ArrayList<>();
        this.players = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public boolean isFriendRequestNotifications() {
        return friendRequestNotifications;
    }

    public void setFriendRequestNotifications(boolean friendRequestNotifications) {
        this.friendRequestNotifications = friendRequestNotifications;
    }

    public boolean isInviteNotifications() {
        return inviteNotifications;
    }

    public void setInviteNotifications(boolean inviteNotifications) {
        this.inviteNotifications = inviteNotifications;
    }

    public boolean isOtherTurnNotifications() {
        return otherTurnNotifications;
    }

    public void setOtherTurnNotifications(boolean otherTurnNotifications) {
        this.otherTurnNotifications = otherTurnNotifications;
    }

    public boolean isMyTurnNotifications() {
        return myTurnNotifications;
    }

    public void setMyTurnNotifications(boolean myTurnNotifications) {
        this.myTurnNotifications = myTurnNotifications;
    }

    public boolean isTurnIsExpiringNotifications() {
        return turnIsExpiringNotifications;
    }

    public void setTurnIsExpiringNotifications(boolean turnIsExpiringNotifications) {
        this.turnIsExpiringNotifications = turnIsExpiringNotifications;
    }

    public boolean isChatNotifications() {
        return chatNotifications;
    }

    public void setChatNotifications(boolean chatNotifications) {
        this.chatNotifications = chatNotifications;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDateTime dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getNumberOfGames() {
        return numberOfGames;
    }

    public void setNumberOfGames(int numberOfGames) {
        this.numberOfGames = numberOfGames;
    }

    public int getNumberOfWins() {
        return numberOfWins;
    }

    public void setNumberOfWins(int numberOfWins) {
        this.numberOfWins = numberOfWins;
    }

    public double getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(double averageScore) {
        this.averageScore = averageScore;
    }

    public int getTopScore() {
        return topScore;
    }

    public void setTopScore(int topScore) {
        this.topScore = topScore;
    }

    public int getReplayLiveDays() {
        return replayLiveDays;
    }

    public void setReplayLiveDays(int replayLiveDays) {
        this.replayLiveDays = replayLiveDays;
    }

    public List<MVPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(List<MVPlayer> players) {
        this.players = players;
    }

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }

    public List<ChatMessage> getChatMessages() {
        return chatMessages;
    }

    public void setChatMessages(List<ChatMessage> chatMessages) {
        this.chatMessages = chatMessages;
    }

    public List<Friend> getFriends() {
        return friends;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return String.format("User: %s", this.username);
    }
}

