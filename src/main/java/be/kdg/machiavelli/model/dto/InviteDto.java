package be.kdg.machiavelli.model.dto;

public class InviteDto {
    private int id;
    private String invitedUserName;
    private String invitingUserName;
    private GameDto gameDto;

    public InviteDto() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInvitedUserName() {
        return invitedUserName;
    }

    public void setInvitedUserName(String invitedUserName) {
        this.invitedUserName = invitedUserName;
    }

    public String getInvitingUserName() {
        return invitingUserName;
    }

    public void setInvitingUserName(String invitingUserName) {
        this.invitingUserName = invitingUserName;
    }

    public GameDto getGameDto() {
        return gameDto;
    }

    public void setGameDto(GameDto gameDto) {
        this.gameDto = gameDto;
    }

    @Override
    public String toString() {
        return "InviteDto{" +
                "id=" + id +
                ", invitedUserName='" + invitedUserName + '\'' +
                ", invitingUserName='" + invitingUserName + '\'' +
                ", gameDto=" + gameDto +
                '}';
    }
}
