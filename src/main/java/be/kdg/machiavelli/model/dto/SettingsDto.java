package be.kdg.machiavelli.model.dto;

public class SettingsDto {
    private String username;
    private boolean friendRequestNotifications;
    private boolean inviteNotifications;
    private boolean otherTurnNotifications;
    private boolean myTurnNotifications;
    private boolean turnIsExpiringNotifications;
    private boolean chatNotifications;
    private boolean isPublic;

    public SettingsDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isFriendRequestNotifications() {
        return friendRequestNotifications;
    }

    public void setFriendRequestNotifications(boolean friendRequestNotifications) {
        this.friendRequestNotifications = friendRequestNotifications;
    }

    public boolean isInviteNotifications() {
        return inviteNotifications;
    }

    public void setInviteNotifications(boolean inviteNotifications) {
        this.inviteNotifications = inviteNotifications;
    }

    public boolean isOtherTurnNotifications() {
        return otherTurnNotifications;
    }

    public void setOtherTurnNotifications(boolean otherTurnNotifications) {
        this.otherTurnNotifications = otherTurnNotifications;
    }

    public boolean isMyTurnNotifications() {
        return myTurnNotifications;
    }

    public void setMyTurnNotifications(boolean myTurnNotifications) {
        this.myTurnNotifications = myTurnNotifications;
    }

    public boolean isTurnIsExpiringNotifications() {
        return turnIsExpiringNotifications;
    }

    public void setTurnIsExpiringNotifications(boolean turnIsExpiringNotifications) {
        this.turnIsExpiringNotifications = turnIsExpiringNotifications;
    }

    public boolean isChatNotifications() {
        return chatNotifications;
    }

    public void setChatNotifications(boolean chatNotifications) {
        this.chatNotifications = chatNotifications;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}
