package be.kdg.machiavelli.model.dto;

import be.kdg.machiavelli.model.game.artifacts.BuildingCard;

import java.util.Arrays;
import java.util.List;

public class GameDto {
    private int id;
    private String gameName;
    private BuildingCardDto[] buildingCards;
    private CharacterCardDto[] characterCards;
    private PlayerDto[] players;
    private int turnDuration;
    private int reqBuildings;
    private int maxPlayers;
    private int nrOfPlayers;
    private int joinType;
    private int status;
    private int playerTurn;
    private int characterTurn;
    private int coinsLeft;
    private boolean playersChosen;
    private boolean gameOver;

    public GameDto() {
    }

    //public GameDto(int id, String gameName, BuildingCardDto[] buildingCards, CharacterCardDto[] characterCards, PlayerDto[] players, int turnDuration, int reqBuildings, int maxPlayers, int nrOfPlayers, int joinType, int status) {
    //    this.id = id;
    //    this.gameName = gameName;
    //    this.buildingCards = buildingCards;
    //    this.characterCards = characterCards;
    //    this.players = players;
    //    this.turnDuration = turnDuration;
    //    this.reqBuildings = reqBuildings;
    //    this.maxPlayers = maxPlayers;
    //    this.nrOfPlayers = nrOfPlayers;
    //    this.joinType = joinType;
    //    this.status = status;
    //}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public int getTurnDuration() {
        return turnDuration;
    }

    public void setTurnDuration(int turnDuration) {
        this.turnDuration = turnDuration;
    }

    public int getReqBuildings() {
        return reqBuildings;
    }

    public void setReqBuildings(int reqBuildings) {
        this.reqBuildings = reqBuildings;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public int getJoinType() {
        return joinType;
    }

    public void setJoinType(int joinType) {
        this.joinType = joinType;
    }

    public int getNrOfPlayers() {
        return nrOfPlayers;
    }

    public void setNrOfPlayers(int nrOfPlayers) {
        this.nrOfPlayers = nrOfPlayers;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public BuildingCardDto[] getBuildingCards() {
        return buildingCards;
    }

    public void setBuildingCards(BuildingCardDto[] buildingCards) {
        this.buildingCards = buildingCards;
    }

    public CharacterCardDto[] getCharacterCards() {
        return characterCards;
    }

    public void setCharacterCards(CharacterCardDto[] characterCards) {
        this.characterCards = characterCards;
    }

    public PlayerDto[] getPlayers() {
        return players;
    }

    public void setPlayers(PlayerDto[] players) {
        this.players = players;
    }

    public int getPlayerTurn() {
        return playerTurn;
    }

    public void setPlayerTurn(int playerTurn) {
        this.playerTurn = playerTurn;
    }

    public int getCoinsLeft() {
        return coinsLeft;
    }

    public void setCoinsLeft(int coinsLeft) {
        this.coinsLeft = coinsLeft;
    }

    public int getCharacterTurn() {
        return characterTurn;
    }

    public void setCharacterTurn(int characterTurn) {
        this.characterTurn = characterTurn;
    }

    public boolean isPlayersChosen() {
        return playersChosen;
    }

    public void setPlayersChosen(boolean playersChosen) {
        this.playersChosen = playersChosen;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    @Override
    public String toString() {
        return "GameDto{" +
                "id=" + id +
                ", gameName='" + gameName + '\'' +
                ", buildingCards=" + Arrays.toString(buildingCards) +
                ", characterCards=" + Arrays.toString(characterCards) +
                ", players=" + Arrays.toString(players) +
                ", turnDuration=" + turnDuration +
                ", reqBuildings=" + reqBuildings +
                ", maxPlayers=" + maxPlayers +
                ", nrOfPlayers=" + nrOfPlayers +
                ", joinType=" + joinType +
                ", status=" + status +
                ", playerTurn=" + playerTurn +
                ", characterTurn=" + characterTurn +
                ", coinsLeft=" + coinsLeft +
                ", playersChosen=" + playersChosen +
                '}';
    }
}
