package be.kdg.machiavelli.model.dto;

public class SearchUsersDto {
    private String username;

    public SearchUsersDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
