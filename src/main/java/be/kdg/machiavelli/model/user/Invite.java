package be.kdg.machiavelli.model.user;

import be.kdg.machiavelli.model.game.logic.MVGame;
import be.kdg.machiavelli.model.security.User;

import javax.persistence.*;

@Entity
public class Invite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(targetEntity = User.class)
    private User invitingUser;
    @ManyToOne(targetEntity = User.class)
    private User invitedUser;
    @ManyToOne(targetEntity = MVGame.class)
    private MVGame game;

    public Invite() {
    }

    public Invite(User invitingUser, User invitedUser, MVGame game) {
        this.invitingUser = invitingUser;
        this.invitedUser = invitedUser;
        this.game = game;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getInvitingUser() {
        return invitingUser;
    }

    public void setInvitingUser(User invitingUser) {
        this.invitingUser = invitingUser;
    }

    public User getInvitedUser() {
        return invitedUser;
    }

    public void setInvitedUser(User invitedUser) {
        this.invitedUser = invitedUser;
    }

    public MVGame getGame() {
        return game;
    }

    public void setGame(MVGame game) {
        this.game = game;
    }
}
